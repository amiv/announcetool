export default () => {
  const wloc = window.location
  return `${wloc.protocol}//${wloc.hostname}${
    wloc.port !== '' ? `:${wloc.port}` : ``
  }`
}
