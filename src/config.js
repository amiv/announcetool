/**
 * Url used for calls to the AMIV API
 */
export const apiUrl = 'https://api.amiv.ethz.ch'
export const loginApiUrl = 'https://api.amiv.ethz.ch'
export const announceApiUrl = 'https://announce-backend.amiv.ethz.ch'

export const oAuthID = 'Announce Tool'
