import React from 'react'
import Grid from '@material-ui/core/Grid'
import PropTypes from 'prop-types'
import { DragDropContext } from 'react-beautiful-dnd'
import DroppableList from './droppableList'

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
/*
child
*/
const DraggableSelectionList = ({ selected, onUpdateSelection, children }) => {
  const onDragEnd = result => {
    const { source, destination } = result

    // dropped outside the list
    if (!destination && source.droppableId === 'selection') {
      const selected_clone = [...selected]
      selected_clone.slice(source.index)
      onUpdateSelection(selected_clone)
    }

    if (!destination) {
      return
    }

    if (
      source.droppableId === 'selection' &&
      destination.droppableId === 'selection'
    ) {
      let selected_clone = [...selected]
      selected_clone = reorder(selected_clone, source.index, destination.index)
      onUpdateSelection(selected_clone)
    }

    if (
      source.droppableId === 'items' &&
      destination.droppableId === 'selection'
    ) {
      const selected_clone = [...selected]
      selected_clone.splice(
        destination.index,
        0,
        children[source.index].props.id
      )
      onUpdateSelection(selected_clone)
    }
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Grid container spacing={2}>
        <Grid item>
          <DroppableList id="items">
            {React.Children.map(
              React.Children.map(children, child =>
                selected.indexOf(child.props.id) === -1 ? child : null
              ),
              (dispalychild, i) =>
                React.cloneElement(dispalychild, { index: i })
            )}
          </DroppableList>
        </Grid>
        <Grid item>
          <DroppableList id="selection">
            {React.Children.map(
              React.Children.map(children, child =>
                selected.indexOf(child.props.id) !== -1 ? child : null
              ),
              (dispalychild, i) => {
                return React.cloneElement(dispalychild, { index: i })
              }
            )}
          </DroppableList>
        </Grid>
      </Grid>
    </DragDropContext>
  )
}

DraggableSelectionList.propTypes = {
  selected: PropTypes.arrayOf(PropTypes.any),
  children: PropTypes.node.isRequired,
  onUpdateSelection: PropTypes.func,
}

DraggableSelectionList.defaultProps = {
  selected: [],
  onUpdateSelection: () => null,
}

export default DraggableSelectionList
