import React from 'react'
import PropTypes from 'prop-types'
import { Droppable } from 'react-beautiful-dnd'
import { List } from '@material-ui/core'

const grid = 8

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? 'lightblue' : 'lightgrey',
  padding: grid,
  width: 450,
})

const DroppableList = props => {
  return (
    <Droppable droppableId={props.id}>
      {(provided, snapshot) => (
        <List
          style={getListStyle(snapshot.isDraggingOver)}
          ref={provided.innerRef}
        >
          {props.children}
          {provided.placeholder}
        </List>
      )}
    </Droppable>
  )
}

DroppableList.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.arrayOf(PropTypes.element),
}

export default DroppableList
