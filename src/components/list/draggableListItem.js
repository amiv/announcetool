import React from 'react'
import PropTypes from 'prop-types'
import {
  ListItem,
  ListItemText,
  ListItemIcon,
  ListItemSecondaryAction,
} from '@material-ui/core'
import { Draggable } from 'react-beautiful-dnd'
import InboxIcon from '@material-ui/icons/Inbox'
import Img from '../newComponents/image'

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer

  // change background colour if dragging
  background: isDragging ? 'lightgreen' : 'lightgrey',

  // styles we need to apply on draggables
  ...draggableStyle,
})

const DraggableListItem = ({ id, index, ...props }) => {
  return (
    <Draggable key={id} draggableId={id} index={index}>
      {(provided, snapshot) => (
        <ListItem
          ContainerComponent="div"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={getItemStyle(
            snapshot.isDragging,
            provided.draggableProps.style
          )}
        >
          {/* <Img src={props.image} ratioX={1} ratioY={1} alt="image" /> */}

          <ListItemText
            primary={props.primary}
            primaryTypographyProps={{ noWrap: true }}
            secondary={props.secondary}
          />
          <ListItemSecondaryAction />
        </ListItem>
      )}
    </Draggable>
  )
}

DraggableListItem.propTypes = {
  id: PropTypes.string,
  index: PropTypes.number,
  primary: PropTypes.string,
  secondary: PropTypes.string,
  image: PropTypes.string,
}

DraggableListItem.defaultProps = {
  secondary: null,
}

export default DraggableListItem
